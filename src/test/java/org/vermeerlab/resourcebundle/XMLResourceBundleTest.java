/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
package org.vermeerlab.resourcebundle;

import java.util.Properties;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author Yamashita,Takahiro
 */
public class XMLResourceBundleTest {

    @Test
    public void インスタンス_カバレージ() {
        XMLResourceBundle xml = new XMLResourceBundle();

        Properties properties = new Properties() {
            private static final long serialVersionUID = 1L;
        };
        XMLResourceBundle xmlWithParam = new XMLResourceBundle(properties);
    }

    @Test(expected = NullPointerException.class)
    public void testHandleGetObject() {
        XMLResourceBundle xml = new XMLResourceBundle();
        xml.handleGetObject(null);
    }

    @Test
    public void testGetKeys() {
        Properties properties = new Properties() {
            private static final long serialVersionUID = 1L;
        };
        XMLResourceBundle xml = new XMLResourceBundle(properties);
        Assert.assertFalse(xml.getKeys().hasMoreElements());
    }
}

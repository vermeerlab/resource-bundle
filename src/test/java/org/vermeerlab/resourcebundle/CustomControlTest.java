/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
package org.vermeerlab.resourcebundle;

import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.Set;
import static org.hamcrest.CoreMatchers.is;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author Yamashita,Takahiro
 */
public class CustomControlTest {

    @Test(expected = IllegalArgumentException.class)
    public void testNewBundle() throws Exception {
        CustomControl control = CustomControl.builder().build();
        ClassLoader loader = this.getClass().getClassLoader();
        control.newBundle("", Locale.ITALY, "dummy2", loader, true);
    }

    @Test
    public void testNewBundleProperties_ResourceBundleIsNull() {
        CustomControl control = CustomControl.builder().build();
        ClassLoader loader = this.getClass().getClassLoader();
        try {
            ResourceBundle bundle = control.newBundleProperties("://", Locale.ROOT, loader, true);
            Assert.assertNull(bundle);
        } catch (IllegalAccessException | InstantiationException | IOException ex) {
            throw new RuntimeException();
        }
    }

    @Test
    public void testNewBundleProperties() {
        CustomControl control = CustomControl.builder().build();
        ClassLoader loader = this.getClass().getClassLoader();
        try {
            ResourceBundle bundle = control.newBundleProperties("message", Locale.ROOT, loader, true);
            Assert.assertNull(bundle);
        } catch (IllegalAccessException | InstantiationException | IOException ex) {
            throw new RuntimeException();
        }
    }

    @Test
    public void concateResourceProperties() {
        CustomControl control = CustomControl.builder().build();
        ResourceBundle bundle = ResourceBundle.getBundle("mergebundle.patternA,  mergebundle.patternB,  mergebundle.patternC,  mergebundle.patternD",
                                                         control);

        Set<String> keySet = bundle.keySet();
        Assert.assertThat(keySet.size(), is(4));
        Assert.assertThat(bundle.getString("a"), is("value is a"));
        Assert.assertThat(bundle.getString("b"), is("value is b"));
        Assert.assertThat(bundle.getString("c"), is("value is c（日本語）"));
        Assert.assertThat(bundle.getString("d"), is("value is d"));

    }

    @Test
    public void concateResourceProperties_setLocala() {
        CustomControl control = CustomControl.builder().build();
        ResourceBundle bundle = ResourceBundle.getBundle("mergebundle.patternA,  mergebundle.patternB,  mergebundle.patternC,  mergebundle.patternD",
                                                         Locale.ITALIAN, control);

        Set<String> keySet = bundle.keySet();
        Assert.assertThat(keySet.size(), is(4));
        Assert.assertThat(bundle.getString("a"), is("value is a イタリア"));
        Assert.assertThat(bundle.getString("b"), is("value is b"));
        Assert.assertThat(bundle.getString("c"), is("value is c"));
        Assert.assertThat(bundle.getString("d"), is("value is d"));

    }

}

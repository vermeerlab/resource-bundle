/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
package org.vermeerlab.resourcebundle;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.SequenceInputStream;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.security.AccessController;
import java.security.PrivilegedActionException;
import java.security.PrivilegedExceptionAction;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.Optional;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;
import java.util.ResourceBundle.Control;
import lombok.Builder;
import lombok.Singular;

/**
 * 通常のCotrolに加えて、文字コードを指定してResourseを取得できるように拡張をしたResourceBundle.Controlクラスです.
 * <P>
 * 冗長的ですが拡張メソッドを把握するため、および その用途を記すために、拡張メソッドを全てOverrideします.
 * <P>
 * 実装例
 * <ul>
 *
 * <li>
 * デフォルト指定.<br>
 * 文字コード ASCII、取得対象検索順：class・properties・xml、Localeはデフォルトロケールです.<br>
 * （デフォルトリソースではありません）<br>
 * デフォルトロケールが存在しない場合は、デフォルトリソース（ロケール記載の無いリソースファイル）を検索します.<br>
 * <pre>
 * {@code
 * CustomControl control = CustomControl.builder().build();
 * }
 * </pre>
 * </li>
 *
 * <li>
 * 文字コードを指定する場合<br>
 * <pre>
 * {@code
 * CustomControl control = CustomControl.builder().charCode(StandardCharsets.UTF_8.toString()).build();
 * CustomControl control = CustomControl.builder().charCode("UTF-8").build();
 * CustomControl control = CustomControl.builder().charCode("SJIS").build();
 * }
 * </pre>
 * </li>
 *
 * <li>
 * 参照するリソースの順番を変える場合<br>
 * デフォルト（class・properties・xml）から properties・xml・classに変更する.<br>
 *
 * <pre>
 * {@code
 * CustomControl control = CustomControl.builder()
 *  .formats(CustomControl.FORMAT_DEFAULT)
 *  .formats(CustomControl.FORMAT_XML)
 *  .build();
 * }
 * </pre>
 * </li>
 *
 * <li>
 * 参照するリソースを固定する場合<br>
 *
 * XMLのみの場合
 * <pre>
 * {@code
 * CustomControl control = CustomControl.builder()
 *  .formats(CustomControl.FORMAT_XML)
 *  .build();
 * }
 * </pre>
 * </li>
 *
 * <li>
 * 検索ロケール順を指定する場合<br>
 * 必ず、デフォルト{@code Locale.ROOT}を入れておくと、リソース検索エラーになりにくいと思われます.<br>
 *
 * 事例は検索を適用するロケールが日本語（JAPANESE）で、検索順をUS・JAPAN・デフォルトリソースの順とした記述（通常、こういうことはしないが実装例として記載）<br>
 * <pre>
 * {@code
 * CustomControl control = CustomControl.builder()
 * .targetCandidateLocalePair(
 *  TargetCandidateLocalePair.builder()
 *      .targetLocale(Locale.JAPANESE)
 *      .candidateLocale(Locale.US)
 *      .candidateLocale(Locale.JAPAN)
 *      .candidateLocale(Locale.ROOT)
 *      .build()
 *  .build();
 * }
 * </pre>
 * <br>
 * 検索を適用するロケールを複数指定した場合<br>
 * <pre>
 * {@code
 * CustomControl control = CustomControl.builder()
 * .charCode("UTF-8")
 * .targetCandidateLocalePair(
 *  TargetCandidateLocalePair.builder()
 *      .targetLocale(Locale.US)
 *      .candidateLocale(Locale.Locale.ENGLISH)
 *      .candidateLocale(Locale.ROOT)
 *      .build()
 * .targetCandidateLocalePair(
 *  TargetCandidateLocalePair.builder()
 *      .targetLocale(Locale.JAPANESE)
 *      .candidateLocale(Locale.JAPAN)
 *      .candidateLocale(Locale.ROOT)
 *      .build()
 *  .build();
 * }
 * </pre>
 *
 * {@code null}の場合は、ResourceBundleで指定したロケールと同じロケールに置き換えます.<br>
 * <pre>
 * {@code
 * CustomControl control = CustomControl.builder()
 * .targetCandidateLocalePair(
 *  TargetCandidateLocalePair.builder()
 *      .targetLocale(Locale.JAPANESE)
 *      .candidateLocale(Locale.US)
 *      .candidateLocale(null)
 *      .candidateLocale(Locale.ROOT)
 *      .build()
 *  .build();
 * }
 * </pre>
 * <br>
 *
 * </li>
 *
 * <li>
 * データキャッシュを設定する場合<br>
 *
 * キャッシュする：CustomControl.TTL_NO_EXPIRATION_CONTROL（デフォルト）<br>
 * <pre>
 * {@code
 * CustomControl control = CustomControl.builder().timeToLive(CustomControl.TTL_NO_EXPIRATION_CONTROL).build();
 * }
 * </pre>
 *
 * キャッシュしない・都度リソースを読み込む：CustomControl.TTL_DONT_CACHE.<br>
 * <pre>
 * {@code
 * CustomControl control = CustomControl.builder().timeToLive(CustomControl.TTL_DONT_CACHE).build();
 * }
 * </pre>
 *
 * ResourceBundleの有効期限(0またはキャッシュ格納時刻からの正のミリ秒オフセット)を指定します.<br>
 * <pre>
 * {@code
 * CustomControl control = CustomControl.builder().timeToLive(1000L).build();
 * }
 * </pre>
 *
 * </li>
 *
 * </ul>
 *
 * 全ての設定は組み合わせて使用することが出来ます.
 *
 *
 * @author Yamashita,Takahiro
 */
@Builder
public class CustomControl extends Control {

    /**
     * formatに使用するXML用の定数
     */
    public static final List<String> FORMAT_XML = Collections.unmodifiableList(Arrays.asList("xml"));

    /**
     * formatに指定可能な全定数
     */
    public static final List<String> FORMAT_ALL = Collections.unmodifiableList(Arrays.asList("java.class",
                                                                                             "java.properties", "xml"));

    private final String charCode;

    private final Long timeToLive;

    @Singular
    private final List<String> formats;

    @Singular
    private final List<TargetCandidateLocalePair> targetCandidateLocalePairs;

    /* fallbackが再帰的に呼び込まれている状態 = 無限ループ */
    @Builder.Default
    private boolean isFallBackInfiniteLoop = false;

    /**
     * 新しいResourceBundleを生成します.
     * <p>
     * propertiesは、charCodeで指定した文字コードでエンコードしながら読み込みます.<br>
     *
     * @see java.util.ResourceBundle.Control#newBundle(java.lang.String, java.util.Locale,
     * java.lang.String,java.lang.ClassLoader, boolean)
     *
     * @param baseName
     * the base bundle name of the resource bundle, a fully qualified class name<br>
     * * 参照対象が"*.properties"の場合に限り、カンマ区切りでリソース名を指定することで複数のリソースを結合して参照できます。
     * @param locale the locale for which the resource bundle should be instantiated
     * @param format
     * the resource bundle format to be loaded
     * @param loader the <code>ClassLoader</code> to use to load the bundle
     * @param reload
     * the flag to indicate bundle reloading; <code>true</code>
     * if reloading an expired resource bundle,
     * <code>false</code> otherwise
     *
     * @return 生成したResourceBundle
     * @throws java.lang.IllegalAccessException 配列以外のインスタンス作成、フィールドの設定または取得、メソッドの呼び出し時に異常があった場合
     * @throws java.lang.InstantiationException 指定されたクラスオブジェクトのインスタンスを生成できなかった場合
     * @throws java.io.IOException resourceファイルの取得時に異常があった場合
     */
    @Override
    public ResourceBundle newBundle(String baseName, Locale locale, String format, ClassLoader loader, boolean reload)
            throws IllegalAccessException, InstantiationException, IOException {

        this.isFallBackInfiniteLoop = false;
        if (CustomControl.FORMAT_CLASS.contains(format)) {
            return this.newBundleClass(baseName, locale, format, loader, reload);
        }

        if (CustomControl.FORMAT_PROPERTIES.contains(format)) {
            return this.newBundleProperties(baseName, locale, loader, reload);
        }

        if (CustomControl.FORMAT_XML.contains(format)) {
            return this.newBundleXML(baseName, locale, format, loader, reload);
        }

        throw new IllegalArgumentException("unknown format: " + format);
    }

    /**
     * Classファイルの読み込みResourceBundleを生成します.
     * <P>
     * baseNameをカンマ区切りにして複数リソースを参照する実装は「未実装」.
     * <P>
     * @see java.util.ResourceBundle.Control#newBundle(java.lang.String, java.util.Locale,
     * java.lang.String,java.lang.ClassLoader, boolean)
     *
     *
     * @param baseName
     * the base bundle name of the resource bundle, a fully
     * qualified class name
     * @param locale the locale for which the resource bundle should be instantiated
     * @param format
     * the resource bundle format to be loaded
     * @param loader the <code>ClassLoader</code> to use to load the bundle
     * @param reload
     * the flag to indicate bundle reloading; <code>true</code>
     * if reloading an expired resource bundle,
     * <code>false</code> otherwise
     * @return 生成したResourceBundle
     * @exception IllegalAccessException
     * if the class or its nullary constructor is not
     * accessible.
     * @exception InstantiationException
     * if the instantiation of a class fails for some other
     * reason.
     * @exception IOException
     * if an error occurred when reading resources using
     * any I/O operations
     */
    protected ResourceBundle newBundleClass(String baseName, Locale locale, String format,
                                            ClassLoader loader, boolean reload)
            throws IllegalAccessException, InstantiationException, IOException {
        String bundleName = toBundleName(baseName, locale);
        ResourceBundle bundle = null;

        try {
            @SuppressWarnings("unchecked")
            Class<? extends ResourceBundle> bundleClass
                                            = (Class<? extends ResourceBundle>) loader.loadClass(bundleName);

            // If the class isn't a ResourceBundle subclass, throw a
            // ClassCastException.
            if (ResourceBundle.class.isAssignableFrom(bundleClass)) {
                bundle = bundleClass.newInstance();
            } else {
                throw new ClassCastException(bundleClass.getName()
                                             + " cannot be cast to ResourceBundle");
            }
        } catch (ClassNotFoundException e) {
        }
        return bundle;
    }

    /**
     * propertiesファイルの読み込みResourceBundleを生成します.
     * <P>
     * @see java.util.ResourceBundle.Control#newBundle(java.lang.String, java.util.Locale,
     * java.lang.String,java.lang.ClassLoader, boolean)
     *
     *
     * @param baseName
     * the base bundle name of the resource bundle, a fully
     * qualified class name
     * @param locale the locale for which the resource bundle should be instantiated
     * @param loader the <code>ClassLoader</code> to use to load the bundle
     * @param reload
     * the flag to indicate bundle reloading; <code>true</code>
     * if reloading an expired resource bundle,
     * <code>false</code> otherwise
     * @return 生成したResourceBundle
     * @exception IllegalAccessException
     * if the class or its nullary constructor is not
     * accessible.
     * @exception InstantiationException
     * if the instantiation of a class fails for some other
     * reason.
     * @exception IOException
     * if an error occurred when reading resources using
     * any I/O operations
     */
    protected ResourceBundle newBundleProperties(String baseName, Locale locale, ClassLoader loader, boolean reload)
            throws IllegalAccessException, InstantiationException, IOException {
        InputStream stream = readBaseNames(baseName, locale, loader, reload);
        ResourceBundle bundle = null;
        if (stream != null) {
            try {
                if (this.charCode != null) {
                    bundle = new PropertyResourceBundle(new InputStreamReader(stream, this.charCode));
                } else {
                    bundle = new PropertyResourceBundle(stream);
                }
            } finally {
                stream.close();
            }
        }
        return bundle;
    }

    InputStream readBaseNames(String baseName, Locale locale, ClassLoader loader, boolean reload) throws IOException {
        String[] _baseNames = baseName.split(",");
        List<InputStream> propertiesInputStreams = new ArrayList<>();
        for (String _baseName : _baseNames) {
            InputStream propertiesInputStream = readProperties(_baseName.trim(), locale, loader, reload);
            /* propertiesの結合時に使用する改行文字 */
            InputStream lineSeparator = new ByteArrayInputStream(System.lineSeparator().getBytes(StandardCharsets.UTF_8));
            if (propertiesInputStream != null) {
                InputStream inputStreamWithLineSeparator = new SequenceInputStream(propertiesInputStream, lineSeparator);
                propertiesInputStreams.add(inputStreamWithLineSeparator);
            }
        }

        if (propertiesInputStreams.isEmpty()) {
            return null;
        }

        Iterator<InputStream> inputStreamsIterator = propertiesInputStreams.iterator();
        InputStream inputStream = inputStreamsIterator.next();
        if (propertiesInputStreams.size() == 1) {
            return inputStream;
        }
        return concateInputStrem(inputStreamsIterator, inputStream);
    }

    InputStream readProperties(String baseName, Locale locale, ClassLoader loader, boolean reload) throws IOException {
        String bundleName = toBundleName(baseName, locale);
        final String resourceName = (bundleName.contains("://"))
                                    ? null
                                    : toResourceName(bundleName, "properties");
        if (resourceName == null) {
            return null;
        }
        final ClassLoader classLoader = loader;
        final boolean reloadFlag = reload;
        InputStream stream = null;
        try {
            stream = AccessController.doPrivileged((PrivilegedExceptionAction<InputStream>) () -> {
                InputStream is = null;
                if (reloadFlag) {
                    URL url = classLoader.getResource(resourceName);
                    if (url != null) {
                        URLConnection connection = url.openConnection();
                        if (connection != null) {
                            // Disable caches to get fresh data for reloading.
                            connection.setUseCaches(false);
                            is = connection.getInputStream();
                        }
                    }
                } else {
                    is = classLoader.getResourceAsStream(resourceName);
                }
                return is;
            });
        } catch (PrivilegedActionException e) {
            throw (IOException) e.getException();
        }
        return stream;
    }

    /**
     * InputStreamを結合して返却します.
     *
     * @param inputStreamsIterator 参照資産のイテレーター
     * @param concatedInputStream 結合先となるInputStream
     * @return 結合したInputStream
     */
    InputStream concateInputStrem(Iterator<InputStream> inputStreamsIterator, InputStream concatedInputStream) throws IOException {
        if (inputStreamsIterator.hasNext() == false) {
            return concatedInputStream;
        }
        InputStream inputStream = inputStreamsIterator.next();
        SequenceInputStream sequenceInputStream = new SequenceInputStream(concatedInputStream, inputStream);
        return concateInputStrem(inputStreamsIterator, sequenceInputStream);
    }

    /**
     * XMLファイルの読み込みResourceBundleを生成します.
     * <P>
     * baseNameをカンマ区切りにして複数リソースを参照する実装は「未実装」.
     * <P>
     * @see java.util.ResourceBundle.Control#newBundle(java.lang.String, java.util.Locale,
     * java.lang.String,java.lang.ClassLoader, boolean)
     *
     * @param baseName
     * the base bundle name of the resource bundle, a fully
     * qualified class name
     * @param locale
     * the locale for which the resource bundle should be instantiated
     * @param format
     * the resource bundle format to be loaded
     * @param loader
     * the <code>ClassLoader</code> to use to load the bundle
     * @param reload
     * the flag to indicate bundle reloading; <code>true</code>
     * if reloading an expired resource bundle,
     * <code>false</code> otherwise
     *
     * @return 生成したResourceBundle
     * @exception IllegalAccessException
     * if the class or its nullary constructor is not
     * accessible.
     * @exception InstantiationException
     * if the instantiation of a class fails for some other
     * reason.
     * @exception IOException
     * if an error occurred when reading resources using
     * any I/O operations
     */
    protected ResourceBundle newBundleXML(String baseName, Locale locale, String format, ClassLoader loader, boolean reload)
            throws IllegalAccessException, InstantiationException, IOException {

        ResourceBundle bundle = null;

        String bundleName = toBundleName(baseName, locale);
        final String resourceName = (bundleName.contains("://"))
                                    ? null
                                    : toResourceName(bundleName, "xml");

        InputStream stream = null;
        if (reload) {
            URL url = loader.getResource(resourceName);
            if (url != null) {
                URLConnection connection = url.openConnection();
                if (connection != null) {
                    // Disable caches to get fresh data for reloading.
                    connection.setUseCaches(false);
                    stream = connection.getInputStream();
                }
            }
        } else {
            stream = loader.getResourceAsStream(resourceName);
        }
        if (stream != null) {
            try (BufferedInputStream bis = new BufferedInputStream(stream)) {
                bundle = XMLResourceBundle.of(bis);
            }
        }
        return bundle;
    }

    /**
     * リソースを取得する優先度を指定します.
     *
     * @param baseName
     * the base bundle name of the resource bundle, a fully
     * qualified class name
     * @param locale
     * the locale for which the resource bundle should be instantiated
     * @return 優先度順のロケールリスト
     * @see java.util.ResourceBundle.Control#getCandidateLocales(java.lang.String, java.util.Locale)
     */
    @Override
    public List<Locale> getCandidateLocales(String baseName, Locale locale) {

        Optional<List<Locale>> candidateLocales = targetCandidateLocalePairs.stream()
                .filter(pair -> pair.getTargetLocale().equals(locale))
                .map(TargetCandidateLocalePair::getCandidateLocales)
                .findAny();

        if (candidateLocales.isPresent() == false) {
            return super.getCandidateLocales(baseName, locale);
        }

        List<Locale> localeSetedCandidateLocales = new ArrayList<>();
        candidateLocales.get().stream()
                .forEachOrdered(candidateLocale -> {
                    localeSetedCandidateLocales.add(candidateLocale == null ? locale : candidateLocale);
                });
        return localeSetedCandidateLocales;
    }

    /**
     * デフォルトリソースを取得します.
     * <P>
     * リソースバンドルの検索時、指定したロケールに対応したリソースバンドルが存在しない場合、
     * デフォルトリソースではなく、デフォルトロケールに対応したリソースバンドルを検索してしまいます.<br>
     * 本対応をしないと意図したリソースではないデフォルトロケールを取得してしまい国際化対応が正しく行われません.<br>
     *
     * @param baseName
     * the base bundle name of the resource bundle, a fully
     * qualified class name
     * @param locale
     * the locale for which the resource bundle should be instantiated
     * @return デフォルトリソースのロケール
     * @see java.util.ResourceBundle.Control#getFallbackLocale(java.lang.String, java.util.Locale)
     */
    @Override
    public Locale getFallbackLocale(String baseName, Locale locale) {
        if (this.isFallBackInfiniteLoop) {
            throw new MissingResourceException(
                    "you set baseName is  [" + baseName + "]. fallback locale, but does not exist baseName resource file. check ResourceBundle.getBundle param 'baseName' and resource file name.",
                    baseName, "");
        }
        this.isFallBackInfiniteLoop = true;
        return Locale.ROOT;
    }

    /**
     * ResourceBundleとして読み込み対象とする分類を返却します.
     * <P>
     * デフォルトの優先度は高い順に、{@literal class > properties > xml} です.<br>
     * 特定のフォーマットを指定した場合は、そちらを採用します.<br>
     * baseNameが同じで拡張子が異なるファイルが存在する場合、フォーマットを指定することで正しく処理できるようになります.<br>
     *
     * @param baseName
     * the base bundle name of the resource bundle, a fully
     * qualified class name
     * @return ResourceBundleとして読み込み対象とする分類リスト
     * @see java.util.ResourceBundle.Control#getFormats(java.lang.String)
     */
    @Override
    public List<String> getFormats(String baseName) {
        if (this.formats.isEmpty()) {
            return Collections.unmodifiableList(FORMAT_ALL);
        }

        List<String> setFormats = (new ArrayList<>(this.formats));
        setFormats.retainAll(FORMAT_ALL);
        if (setFormats.isEmpty()) {
            throw new IllegalArgumentException("unknown format: " + this.formats.toString());
        }

        return Collections.unmodifiableList(this.formats);
    }

    /**
     * キャッシュ内のロード済みバンドルの有効期限を返却します.
     * <P>
     * キャッシュ内のロード済みバンドルに有効期限を設ける場合は その時間(0またはキャッシュ格納時刻からの正のミリ秒オフセット)、
     * 有効期限制御を無効にする場合はTTL_NO_EXPIRATION_CONTROL、キャッシュを無効にする場合はTTL_DONT_CACHE を指定してください.
     * <br>
     *
     * @param baseName
     * the base bundle name of the resource bundle, a fully
     * qualified class name
     * @param locale
     * the locale for which the resource bundle should be instantiated
     * @return バンドルの有効期限
     * @see java.util.ResourceBundle.Control#getTimeToLive(java.lang.String, java.util.Locale)
     */
    @Override
    public long getTimeToLive(String baseName, Locale locale) {
        return this.timeToLive == null
               ? super.getTimeToLive(baseName, locale)
               : this.timeToLive;
    }

    /**
     * キャッシュ再ロード判定をします.
     * <P>
     * キャッシュ内で有効期限の切れたbundleの再ロード要否を、loadTimeに指定されたロード時刻や その他のいくつかの条件に基づいて判定します（継承元クラスのコメント抜粋）.<br>
     * （拡張仕様が無いので継承元の操作をそのまま行います）<br>
     *
     * @param baseName
     * the base bundle name of the resource bundle, a fully
     * qualified class name
     * @param locale
     * the locale for which the resource bundle should be instantiated
     * @param format
     * the resource bundle format to be loaded
     * @param loader
     * the <code>ClassLoader</code> to use to load the bundle
     * @param bundle
     * the resource bundle instance that has been expired
     * in the cache
     * @param loadTime
     * the time when <code>bundle</code> was loaded and put
     * in the cache
     * @return キャッシュ内で有効期限の切れたbundleを再ロード要否
     * @see java.util.ResourceBundle.Control#needsReload(java.lang.String, java.util.Locale,
     * java.lang.String, java.lang.ClassLoader, java.util.ResourceBundle, long)
     */
    @Override
    public boolean needsReload(String baseName, Locale locale, String format, ClassLoader loader, ResourceBundle bundle, long loadTime) {
        // ここを実装
        return super.needsReload(baseName, locale, format, loader, bundle, loadTime);
    }

    /**
     * BaseNameとlocaleの組み合わせで取得対象となるプロパティファイル名を編集して返却します.
     * <P>
     * （拡張仕様が無いので継承元の操作をそのまま行います）<br>
     *
     * @param baseName
     * the base bundle name of the resource bundle, a fully
     * qualified class name
     * @param locale
     * the locale for which the resource bundle should be
     * instantiated
     * @return 取得対象のプロパティファイル名
     * @see java.util.ResourceBundle.Control#toBundleName(java.lang.String, java.util.Locale)
     */
    @Override
    public String toBundleName(String baseName, Locale locale) {
        // ここを実装
        return super.toBundleName(baseName, locale);
    }
}

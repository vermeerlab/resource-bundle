/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2017 Yamashita,Takahiro
 */
package org.vermeerlab.resourcebundle;

import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.Properties;
import java.util.ResourceBundle;

/**
 * XML形式のResourceBundleクラス.<br>
 *
 * XMLのフォーマット形式（http://java.sun.com/dtd/properties.dtd）
 *
 * <pre>
 * {@code
 * <!--
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * -->
 *
 * <!-- DTD for properties -->
 *
 * <!ELEMENT properties ( comment?, entry* ) >
 *
 * <!ATTLIST properties version CDATA #FIXED "1.0">
 *
 * <!ELEMENT comment (#PCDATA) >
 *
 * <!ELEMENT entry (#PCDATA) >
 *
 * <!ATTLIST entry key CDATA #REQUIRED>
 *
 * }
 * </pre>
 *
 * @author Yamashita,Takahiro
 */
public class XMLResourceBundle extends ResourceBundle {

    private final Properties properties;

    /**
     * for extention
     */
    protected XMLResourceBundle() {
        this.properties = null;
    }

    /**
     * インスタンスを構築します
     *
     * @param properties resourceから読み込んだproperties
     */
    protected XMLResourceBundle(Properties properties) {
        this.properties = properties;
    }

    /**
     * インスタンスを構築します.
     * <P>
     * Propertiesクラスを使用してXMLファイルを読み込みます.
     *
     * @param stream プロパティファイルのInputStream
     * @return 構築したインスタンス
     * @throws IOException InputStreamの入出力に異常があった場合
     */
    public static XMLResourceBundle of(InputStream stream) throws IOException {
        Properties _properties = new Properties();
        _properties.loadFromXML(stream);
        return new XMLResourceBundle(_properties);
    }

    @Override
    public Object handleGetObject(String key) {
        if (key == null) {
            throw new NullPointerException();
        }
        return properties.get(key);
    }

    @Override
    @SuppressWarnings("unchecked")
    public Enumeration<String> getKeys() {
        return (Enumeration<String>) properties.propertyNames();
    }
}
